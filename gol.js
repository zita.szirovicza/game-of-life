const colors = require('colors');

const generate2D = (column, rows) => {
  const arr = new Array(column);
  for (let i = 0; i < column; i++) {
    arr[i] = new Array(rows);
  }
  return arr;
};

const fill2D = (array) => {
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array[i].length; j++) {
      array[i][j] = Math.floor(Math.random() * 2);
    }
  }
};

const column = 80;
const rows = 80;

const neighborlife = (array, x, y) => {
  let sum = 0;
  for (let i = -1; i < 2; i++) {
    for (let j = -1; j < 2; j++) {
      const col = (x + i + column) % column;
      const crows = (j + y + rows) % rows;
      sum += array[col][crows];
    }
  }
  sum -= array[x][y];
  return sum;
};

const board = generate2D(column, rows);
fill2D(board);
const print2D = (array) => {
  let line = ' ';
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array[i].length; j++) {
      if (array[i][j] === 1) {
        line += ' 1'.white.bgWhite;
      } else {
        line += '  '.black.bgBlack;
      }
    }
    line += '\n';
  }
  console.log(line);
};

const nextlives = (array) => {
  const next = generate2D(column, rows);
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array[i].length; j++) {
      const current = array[i][j];
      if (current === 1 && neighborlife(array, i, j) < 2) {
        next[i][j] = 0;
      } else if (current === 1 && neighborlife(array, i, j) > 3) {
        next[i][j] = 0;
      } else if (current === 0 && neighborlife(array, i, j) === 3) {
        next[i][j] = 1;
      } else {
        next[i][j] = current;
      }
    }
  }
  return next;
};

const game = (array) => {
  console.clear();
  print2D(array);
  nextlives(array);
  const nextgen = nextlives(array);
  setTimeout(() => {
    game(nextgen);
  }, 100);
};
game(board);
